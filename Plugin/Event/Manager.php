<?php


namespace Gamma\ITP\Plugin\Event;


use Psr\Log\LoggerInterface;
use \Magento\Framework\Event\ManagerInterface;

class Manager
{
    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function beforeDispatch(ManagerInterface $manager ,$eventName, array $data){
        $this->logger->info(
            __('Event Requested; %1', $eventName)
        );

        return [$eventName, $data];
    }
}