<?php


namespace Gamma\ITP\Observer;

use Magento\Framework\App\Request\Http as App_Http;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Psr\Log\LoggerInterface;

class Http implements ObserverInterface
{
    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function execute(Observer $observer)
    {
        /** @var App_Http $request */
        $request = $observer->getEvent()->getData('request');

        $this->logger->info(
            __('Route requested; %1', $request->getFullActionName())
        );
    }
}