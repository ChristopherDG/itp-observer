<?php


namespace Gamma\ITP\Homework\Tests;


use Gamma\ITP\Api\Data\TestCaseInterfaceFactory;
use Gamma\ITP\Api\HomeworkTesterInterface;

class RotateImage
{
    const TEST_CASE_DATA = [
        [
            'function' => 'rotateClockwise',
            'arguments' => [[
                [1, 2, 3],
                [4, 5, 6],
                [7, 8, 9]
            ]],
            'expected' => [
                [7, 4, 1],
                [8, 5, 2],
                [9, 6, 3]
            ]
        ],
        [
            'function' => 'rotateClockwise',
            'arguments' => [[
                [5, 1, 9, 11],
                [2, 4, 8, 10],
                [13, 3, 6, 7],
                [15, 14, 12, 16]
            ]],
            'expected' => [
                [15, 13, 2, 5],
                [14, 3, 4, 1],
                [12, 6, 8, 9],
                [16, 7, 10, 11]
            ]
        ],
        [
            'function' => 'rotateClockwise',
            'arguments' => [[
                [255, 0, 0],
                [0, 255, 0],
                [0, 0, 255],
            ]],
            'expected' => [
                [0, 0, 255],
                [0, 255, 0],
                [255, 0, 0],
            ]
        ]
    ];

    /**
     * @var HomeworkTesterInterface
     */
    protected $tester;

    /**
     * @var TestCaseInterfaceFactory
     */
    protected $testCaseInterfaceFactory;

    /**
     * @var \Gamma\ITP\Homework\RotateImage
     */
    protected $rotateImage;

    public function __construct(
        HomeworkTesterInterface $tester,
        TestCaseInterfaceFactory $testCaseInterfaceFactory,
        \Gamma\ITP\Homework\RotateImage $rotateImage
    )
    {

        $this->tester = $tester;
        $this->testCaseInterfaceFactory = $testCaseInterfaceFactory;
        $this->rotateImage = $rotateImage;
    }

    public function test()
    {
        $testCases = array_map(function ($testCaseData) {
            $testCase = $this->testCaseInterfaceFactory->create();
            return $testCase->setFunctionName($testCaseData['function'])
                ->setExpected($testCaseData['expected'])
                ->setArguments($testCaseData['arguments']);
        }, self::TEST_CASE_DATA);

        $this->tester->run($this->rotateImage, $testCases);
    }
}